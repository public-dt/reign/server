const express = require('express');

const post = require('./post');

const routes = express.Router();

routes.use('/post', post);

module.exports = routes;
