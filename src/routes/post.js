const express = require('express');
const axios = require('axios');

const { postController } = require('../controllers');

const postHandler = postController({ axios });

const route = express.Router();

route.get('/', postHandler.listPost);
route.put('/:id', postHandler.deletePost);

module.exports = route;
