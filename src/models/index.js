const mongoose = require('mongoose');

const Post = require('./Post');

mongoose.set('debug', true);

module.exports = {
  Post,
};
