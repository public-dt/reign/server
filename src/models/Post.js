const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema(
  {
    isPublished: {
      type: Boolean,
      default: true,
    },
    created_at: {
      type: Date,
    },
    title: {
      type: String,
    },
    url: {
      type: String,
    },
    author: {
      type: String,
    },
    points: {
      type: String,
    },
    story_text: {
      type: String,
    },
    comment_text: {
      type: String,
    },
    num_comments: {
      type: String,
    },
    story_id: {
      type: Number,
    },
    story_title: {
      type: String,
    },
    story_url: {
      type: String,
    },
    parent_id: {
      type: Number,
    },
    created_at_i: {
      type: Number,
    },
    _tags: [],
    objectID: {
      type: Number,
    },
    _highlightResult: {},
  },
  {
    timestamps: false,
  },
);

module.exports = mongoose.model('Post', PostSchema);
