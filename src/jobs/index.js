const cron = require('node-cron');

const getPostEveryHour = (postHandler) => cron.schedule('0 */1 * * *', () => {
  console.log('Get Post Every Hour');
  postHandler.getPost();
});

module.exports = { getPostEveryHour };
