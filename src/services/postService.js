const { Post } = require('../models');

const savePost = async (data) => {
  const postSaved = await Post.find({}).exec();

  const postSavedArray = postSaved
    .map((post) => post.objectID);

  const filterHits = data
    .filter((hit) => !postSavedArray.includes(Number(hit.objectID)));

  const posts = await Post.insertMany(filterHits)
    .then((hit) => {
      console.log(hit);
      return true;
    })
    .catch((err) => {
      console.log(err);
      return false;
    });

  return posts;
};

const getList = async () => {
  const posts = await Post.find({}).exec();

  const postPublished = posts.filter((post) => post.isPublished);

  console.log(postPublished);
  return postPublished;
};

const deletePost = async (id, isPublished) => {
  const post = await Post.findOneAndUpdate(
    { objectID: id },
    { isPublished },
    { new: true },
  );

  return post;
};

module.exports = {
  getList,
  savePost,
  deletePost,
};
