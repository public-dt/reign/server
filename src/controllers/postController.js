const { postService } = require('../services');

const postController = ({ axios }) => ({
  getPost: async () => {
    const { data } = await axios.get(
      'http://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );

    const { hits } = data;

    const posts = await postService.savePost(hits);

    return posts;
  },
  listPost: async (req, res) => {
    const list = await postService.getList();
    return res.status(200).json(list);
  },
  deletePost: async (req, res) => {
    const { id } = req.params;
    const { isPublished } = req.body;

    const post = await postService
      .deletePost(id, isPublished)
      .then((result) => {
        console.log(result);
        return res.send({
          success: true,
          message: 'Posts was deleted successful',
        });
      })
      .catch((err) => {
        console.log(err);
        return res.send({
          success: false,
          message: 'Error to delete post',
        });
      });

    return post;
  },
});

module.exports = postController;
