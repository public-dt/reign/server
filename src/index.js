const express = require('express');
const mongoose = require('mongoose');
const axios = require('axios');
const cors = require('cors');

const app = express();
app.use(cors());

const { postController } = require('./controllers');

const routes = require('./routes');
const { getPostEveryHour } = require('./jobs');

const postHandler = postController({ axios });

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/', routes);

const URL_MONGODB = 'mongodb://mongo:27017/reign-test';

/* Get post on start app */
postHandler.getPost();
/* Job for get post Hourly */
getPostEveryHour(postHandler);

const port = 3000;
app.listen(port, (err) => {
  if (err) throw err;

  return mongoose
    .connect(URL_MONGODB, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
    })
    .then(() => {
      console.log(`Server listening on port ${port}`);
    })
    .catch((error) => {
      throw error;
    });
});
