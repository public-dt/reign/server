const mongoose = require("mongoose");

const dbHandler = require("./utils/db-handler");
const { postController } = require("../src/controllers");
const { Post } = require("../src/models");

beforeAll(async () => await dbHandler.connect());

afterEach(async () => await dbHandler.clearDatabase());

afterAll(async () => await dbHandler.closeDatabase());

describe("Posts ", () => {
  describe("getPost()", () => {
    it("Can be created correctly", async () => {
      const axios = {
        get: jest.fn().mockResolvedValue({ data: posts })
      };
  
      const controller = await postController({ axios }).getPost();
  
      expect(controller).toBe(true)
      expect(async () => controller).not.toThrow();
    });
  })
  describe("listPost()", () => {
    it("Listing posts", async () => {
      const axios = {
        get: jest.fn().mockResolvedValue({ data: posts })
      };
      const req = {}
      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn()
      }
      
      const controller = await postController({ axios }).listPost(req, res);
      
      expect(res.status.mock.calls).toEqual([[200]])
      expect(res.json.mock.calls).toEqual([[[]]])
      expect(async () => controller).not.toThrow();
    });
  })
  describe("deletePost()", () => {
    it("Deleting post", async () => {
      const axios = {
        get: jest.fn().mockResolvedValue({ data: posts })
      };

      const req = {
        params: jest.fn(),
        body: jest.fn(),

      }
      const res = {
        send: jest.fn()
      }
  
      const controller = await postController({ axios }).deletePost(req, res);
      
      expect(res.send.mock.calls).toEqual([ [ { success: true, message: 'Posts was deleted successful' } ] ])
      expect(async () => controller).not.toThrow();
    });
  })
});

const posts = {
  hits: [
    {
      _tags: ["comment", "author_asdff", "story_22508784"],
      _id: "5e657be8d18f1d3ef1ee2bb7",
      created_at: "2020-03-08T22:44:48.000Z",
      title: null,
      url: null,
      author: "asdff",
      points: null,
      story_text: null,
      comment_text:
        "The problem is that living in the countryside adds another layer of distribution, and it&#x27;s the most inefficient layer given our direct to door expectations with receiving goods these days.<p>Some things come from the countryside, but not everything is produced in the same countryside, so you still need far off goods no matter where you live. However, everything comes to the city to be distributed regionally. Cities are huge nodes in our networks of economies. Every piece of whatever ordered from China, for example, has to first come through the port of LA.<p>So, it is better to live near that port, and where all of the disparate goods from the countryside are brought for market, than to live out in the countryside and demand these goods in existence right now in the city be hand delivered to your door in unincorporated New Mexico or wherever, one item at a time.<p>It is more efficient to live densely. You need to cover less ground to run utilites and provide services, and transportation costs to the environment are much lower. Human activity scars the land, and it is also better to keep those scars centered into one gash than to scar the entirety of the natural landscape so everyone can have their 100&#x27;x 250&#x27; parcel and not have to rub shoulders in an elevator.",
      num_comments: null,
      story_id: 22508784,
      story_title: "The American Frontier Continues to Shape Us (2018)",
      story_url:
        "http://www.bu.edu/articles/2018/the-american-frontier-shapes-us-today-bu-researchers-say/",
      parent_id: 22518731,
      created_at_i: 1583707488,
      objectID: 22521165,
      _highlightResult: {
        author: {
          value: "asdff",
          matchLevel: "none",
          matchedWords: []
        },
        comment_text: {
          value:
            "The problem is that living in the countryside adds another layer of distribution, and it's the most inefficient layer given our direct to door expectations with receiving goods these days.<p>Some things come from the countryside, but not everything is produced in the same countryside, so you still need far off goods no matter where you live. However, everything comes to the city to be distributed regionally. Cities are huge <em>nodes</em> in our networks of economies. Every piece of whatever ordered from China, for example, has to first come through the port of LA.<p>So, it is better to live near that port, and where all of the disparate goods from the countryside are brought for market, than to live out in the countryside and demand these goods in existence right now in the city be hand delivered to your door in unincorporated New Mexico or wherever, one item at a time.<p>It is more efficient to live densely. You need to cover less ground to run utilites and provide services, and transportation costs to the environment are much lower. Human activity scars the land, and it is also better to keep those scars centered into one gash than to scar the entirety of the natural landscape so everyone can have their 100'x 250' parcel and not have to rub shoulders in an elevator.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"]
        },
        story_title: {
          value: "The American Frontier Continues to Shape Us (2018)",
          matchLevel: "none",
          matchedWords: []
        },
        story_url: {
          value:
            "http://www.bu.edu/articles/2018/the-american-frontier-shapes-us-today-bu-researchers-say/",
          matchLevel: "none",
          matchedWords: []
        }
      },
      __v: 0
    }
  ]
};
